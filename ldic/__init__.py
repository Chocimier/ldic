#!/usr/bin/env python3
import functools
import icu
import itertools
import json
import wiktionary
from genshi import Markup
from genshi.builder import Element, Fragment
from genshi.template import TemplateLoader
from genshi.template.loader import TemplateNotFound
from wiktionary.types import Sections


def prefixes(word):
    for i in range(len(word), 0, -1):
        yield word[:i]


def sort_key_starting_with(elem, featured):
    try:
        return featured.index(elem)
    except ValueError:
        return len(featured)


def first_not_none(iterable):
    return next((i for i in iterable if i is not None), None)


def separate_list(iterable, middle, *, first=None, last=None, onlytwo=None):
    onlytwo = first_not_none([onlytwo, last, first, middle])
    if first is None:
        first = middle
    if last is None:
        last = middle
    iterator = iter(iterable)
    up_to_three = list(itertools.islice(iterator, 3))
    if len(up_to_three) == 3:
        buffered = up_to_three[2]
        result = '{}{}{}'.format(up_to_three[0], first, up_to_three[1])
    elif len(up_to_three) == 2:
        return '{}{}{}'.format(up_to_three[0], onlytwo, up_to_three[1])
    elif len(up_to_three) == 1:
        return '{}'.format(up_to_three[0])
    else:
        return ''
    for elem in iterator:
        result += '{}{}'.format(middle, buffered)
        buffered = elem
    result += '{}{}'.format(last, buffered)
    return result


class LocalDictionary:
    _DEFAULT_SECTIONS = (
        Sections.PRONUNCIATION,
        Sections.MEANINGS,
        Sections.TRANSLATIONS,
        Sections.SYNONYMS,
    )

    def __init__(self, data_source):
        self._data_source = data_source
        self._lang = data_source.metadata('lang')
        collation_lang = data_source.metadata('collation')
        if collation_lang is not None:
            collator = icu.Collator.createInstance(icu.Locale(collation_lang))
            self._collation_key = collator.getSortKey
        else:
            self._collation_key = lambda x: x
        self._section_loader = TemplateLoader(
            f'templates/{self._lang}/sections',
            auto_reload=True
        )
        self._hyperlinks = self._load_hyperlinks()

    def _load_hyperlinks(self):
        with open(f'data/{self._lang}/hyperlinks.json', encoding='utf-8') as file:
            return json.loads(file.read())

    def _meanings(self, word, sections):
        result = ''
        wikicode = self._data_source.wikicode(word)
        if wikicode:
            result = self._html_meanings(word, wikicode, sections)
        return result

    def _section_template(self, section):
        try:
            return self._section_loader.load(section + '.html')
        except TemplateNotFound:
            return self._section_loader.load('generic.html')

    def _sort_langs(self, langs, featured):
        tmp = list(langs)
        tmp.sort(key=self._collation_key)
        tmp.sort(key=functools.partial(
            sort_key_starting_with,
            featured=featured
        ))
        return tmp

    def _html_meanings(self, word, wikitext, sections):
        result = Fragment()
        page_parser = wiktionary.WikiPageParser(wikitext, self._lang, word)
        section_parser = wiktionary.WikiSectionParser()
        parsed = page_parser.get_parsed()
        keys = self._sort_langs(parsed.langs.keys(), parsed.featured_langs)
        for lang in keys:
            result += Element('dt')(lang)
            html_sections = Element('dd')
            for i in sections:
                try:
                    section, raw_section = parsed.langs[lang].section(i)
                except KeyError:
                    continue
                parsed_section = Markup(section_parser.parse(raw_section, word))
                template = self._section_template(section.value)
                html_sections.append(template.generate(content=parsed_section))
            result += html_sections
        return result

    def page(self, word, root_url='', search_method='POST',
             sections=_DEFAULT_SECTIONS):
        word = word.strip()
        parameters = {
            'word': word,
            'root_url': root_url,
            'search_method': search_method,
        }
        main_content = Markup(self._meanings(word, sections))
        if not main_content:
            prefixes_json = self._data_source.metadata('prefixes')
            try:
                prefixes = json.loads(prefixes_json)
            except TypeError:
                prefixes = None
            if prefixes and (not word or word[0] not in prefixes):
                parameters['prefixes'] = separate_list(prefixes, middle=', ', last=' i ')
        if word:
            templatefile = 'word.html'
            parameters['hyperlinks'] = self._hyperlinks
            parameters['similars'] = self._similar(word)
            parameters['content'] = main_content
        else:
            templatefile = 'empty.html'
        loader = TemplateLoader('templates', auto_reload=True)
        template = loader.load(templatefile)
        return template.generate(**parameters).render('html', doctype='html5')

    def _similar(self, word):
        word_prefixes = prefixes(word)
        transforms = [lambda x: x, lambda x: x.lower(), lambda x: x.title()]
        transformed = [f(p) for p in word_prefixes for f in transforms]
        existing = self._data_source.existing_pages(transformed)
        return sorted(similar for similar in existing if page != similar)


def page(word, data_source):
    dictionary = LocalDictionary(data_source)
    return dictionary.page(word)


def main():
    import sys
    from .datasources import SqliteDataSource
    data_source = SqliteDataSource(sys.argv[1])
    print(page(sys.argv[2], data_source))


if __name__ == '__main__':
    main()
