import sqlite3


class SqliteDataSource:
    def __init__(self, filename):
        self._db = sqlite3.connect(filename)
        self._cursor = self._db.cursor()

    def metadata(self, key):
        self._cursor.execute('select value from metadata where key = ?', (key,))
        row = self._cursor.fetchone()
        try:
            return row[0]
        except TypeError:
            return None

    def wikicode(self, page_name):
        self._cursor.execute('select wikicode from definitions where word = ?',
            (page_name,))
        row = self._cursor.fetchone()
        try:
            return row[0]
        except TypeError:
            return None

    def existing_pages(self, asked):
        placeholders = ','.join('?' * len(asked))
        query = ('select word from definitions where word in ({})'
            .format(placeholders))
        self._cursor.execute(query, asked)
        rows = self._cursor.fetchall()
        return (row[0] for row in rows)

    def set_wikicode(self, page_name, new_code):
        query = 'insert or replace into definitions (word, wikicode) values (?,?)'
        self._cursor.execute(query, (page_name, new_code))

    def save(self):
        self._db.commit()
