#!/usr/bin/env python3
import itertools
import json
import logging
import logging.config
import sqlite3
import sys
import unicodedataplus

from wiktionary.dump import ArticleHandler, XmlWiktionaryWalker


class PerNameFileHandler(logging.Handler):
    def __init__(self, *args, **kwargs):
        super(PerNameFileHandler, self).__init__(*args, **kwargs)
        self._stored_formatter = None

    def handle(self, record):
        handler = logging.FileHandler(record.name + '.log')
        handler.setFormatter(self._stored_formatter)
        handler.handle(record)

    def setFormatter(self, fmt):
        self._stored_formatter = fmt


class SqliteCreator(ArticleHandler):
    def __init__(self, *, lang=None, collation=None, filename='dictionary.sqlite3'):
        super().__init__()
        self._db = sqlite3.connect(filename)
        self._cursor = self._db.cursor()
        self._cursor.execute('''CREATE TABLE definitions (
            word TEXT NOT NULL UNIQUE,
            wikicode TEXT NOT NULL,
            PRIMARY KEY(word)
        );''')
        self._cursor.execute('''CREATE TABLE metadata (
            key TEXT NOT NULL,
            value TEXT NOT NULL
        );''')
        query = 'INSERT INTO metadata (key, value) VALUES (?, ?)'
        if lang is not None:
            self._cursor.execute(query, ('lang', lang))
        if collation is not None:
            self._cursor.execute(query, ('collation', collation))

    def article(self, title, get_parsed, raw):
        del get_parsed
        query = 'INSERT INTO definitions (word, wikicode) VALUES (?, ?)'
        self._cursor.execute(query, (title, raw))

    def no_more_articles(self):
        self._db.commit()

class DemoSqliteCreator(SqliteCreator):
    _prefixes = ['L', 'l', 'Λ', 'λ', 'Л', 'л', 'Լ', 'լ', 'ל', 'ل', 'ܠ', 'ލ', 'ࡋ',
        'ल', 'ল', 'ਲ', 'લ', 'ର', 'ல', 'ల', 'ಲ', 'ല', 'ල', 'ล', 'ລ', 'ལ',
        'လ', 'ლ', 'ል', 'Ꮃ', 'ᓕ', 'ᛚ', 'ល', 'ᠯ', 'ᦜ', 'ᦟ', 'ᨒ', 'Ⰾ', 'Ⲗ',
        'ⲗ', 'ⵍ', 'ら', 'ラ', 'ꦭ', '알', '𐌻', '𐎾', '𐤋', '𑀮', '𒀀',
        '𞤂', '𞤤']
    _ignored_scripts = ['Common', 'Han', 'Unknown', 'Avestan']
    _rare_scripts = ['Adlam', 'Brahmi', 'Buginese',
        'Canadian_Aboriginal', 'Cherokee', 'GlagoliticCoptic',
        'Javanese', 'Mandaic', 'Mongolian', 'New_Tai_Lue',
        'Old_Persian', 'Runic', 'Syriac', 'Thaana']
    _scripts = set(itertools.chain(
        _ignored_scripts,
        (unicodedataplus.script(i) for i in _prefixes)
    ))

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._words = {i: [] for i in self._prefixes}
        query = 'INSERT INTO metadata (key, value) VALUES (?, ?)'
        self._cursor.execute(query, ('prefixes', json.dumps(self._prefixes)))

    def article(self, title, get_parsed, raw):
        initial = title[0]
        if initial in self._prefixes:
            super().article(title, get_parsed, raw)
            self._words[initial] = self._words[initial][:1] + [title]
        script = unicodedataplus.script(initial)
        if script not in self._scripts:
            logging.getLogger('demo.new-script').warning(
                'script {}: ({}) {}'.format(script, initial, title))
        if script in self._rare_scripts:
            try:
                name = unicodedataplus.name(initial)
            except ValueError:
                name = 'Unnamed Character'
            logging.getLogger('demo.rare-script').info(
                '({}) {}, {}'.format(initial, name, title))

    def no_more_articles(self):
        super().no_more_articles()
        tuples = [(i, unicodedataplus.script(i), self._words.get(i))
                  for i in self._prefixes]
        for i in sorted(tuples, key=lambda x: x[1]):
            logging.getLogger('demo.scripts-recap').info(str(i))


class AllParser(ArticleHandler):
    def __init__(self):
        super().__init__()

    def article(self, title, get_parsed, raw):
        del title, raw
        get_parsed()


def configure_logging():
    try:
        config = open('logging.json')
    except OSError:
        config = open('logging.default.json')
    logging.config.dictConfig(json.load(config))
    config.close()


ACTIONS = {
    'full': lambda args: SqliteCreator(lang=args.lang, collation=args.lang),
    'demo': lambda args: DemoSqliteCreator(lang=args.lang, collation=args.lang),
    'parse': lambda args: AllParser(),
}

def parse_args(args):
    import argparse
    argparser = argparse.ArgumentParser()
    argparser.add_argument('--action', choices=ACTIONS.keys())
    argparser.add_argument('lang')
    argparser.add_argument('file')
    return argparser.parse_args(args)

def main():
    configure_logging()
    args = parse_args(sys.argv[1:])
    handler = ACTIONS[args.action or 'full'](args)
    walker = XmlWiktionaryWalker(xml_path=args.file, scheme=args.lang,
        handler=handler)
    walker.walk()
