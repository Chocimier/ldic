import os
import xml.sax
from . import WikiPageParser


class BreakException(Exception):
    pass


class WiktionaryWalker:
    def __init__(self, scheme=None, handler=None):
        super().__init__()
        self._counter = 0
        self.limit = float('inf')
        self.handler = handler
        self.scheme = scheme

    def walk(self):
        pass

    def article(self, title, raw, *, get_parsed=None):
        self._counter += 1
        if get_parsed is None:
            get_parsed = lambda: WikiPageParser(raw, self.scheme, title=title).get_parsed()
        if self.handler:
            self.handler.article(title, get_parsed, raw)
        if self._counter == self.limit:
            raise BreakException

    def no_more_articles(self):
        if self.handler:
            self.handler.no_more_articles()


class FilesWiktionaryWalker(WiktionaryWalker):
    def __init__(self, scheme=None, handler=None, *, dirname='', suffix='.wiki.txt'):
        super().__init__(scheme=scheme, handler=handler)
        self.dirname = dirname
        self.suffix = suffix
        self.dirname = dirname

    def walk(self):
        self.visit_directory(self.dirname)

    def visit_directory(self, dirname):
        files = os.listdir(dirname)
        for file in files:
            try:
                text = open(os.path.join(dirname, file)).read()
            except IsADirectoryError:
                self.visit_directory(os.path.join(dirname, file))
                continue
            title = self.decode_name(file)
            suffix = self.suffix
            if title.endswith(suffix):
                title = title[:-len(suffix)]
                self.article(title, text)

    _encoded = {
        '\0': '_nul_',
        '/': '_slash_',
        '_': '_uscore_',
    }

    _decoded = {v: k for k, v in _encoded.items()}

    def encode_name(self, name):
        result = ''
        for i in name:
            result += self._encoded.get(i, i)
        return result

    def decode_name(self, name):
        result = ''
        escape = ''
        for i in name:
            if i == '_':
                escape += i
                if len(escape) > 1:
                    result += self._decoded.get(escape, escape)
                    escape = ''
            else:
                if escape:
                    escape += i
                else:
                    result += i
        return result


class XmlWiktionaryWalker(WiktionaryWalker):
    def __init__(self, scheme=None, handler=None, *, xml_path=None):
        super().__init__(scheme=scheme, handler=handler)
        self.xml_path = xml_path

    def walk(self):
        parser = xml.sax.make_parser()
        xml_handler = XmlWiktionaryHandler(self)
        parser.setContentHandler(xml_handler)
        parser.parse(open(self.xml_path, 'r'))


class XmlWiktionaryHandler(xml.sax.ContentHandler):
    _ARTICLE_NS = '0'

    def __init__(self, walker):
        super().__init__()
        self._walker = walker
        self._elements_stack = []
        self._page_data = None
        self._clean_data()

    def _clean_data(self):
        self._page_data = {i: '' for i in ('text', 'title', 'ns')}

    def _current_elem(self):
        try:
            return self._elements_stack[-1]
        except KeyError:
            return None

    def characters(self, content):
        current = self._current_elem()
        if current in ('ns', 'title', 'text'):
            self._page_data[current] += content

    def startElement(self, name, attrs):
        if name == 'page':
            self._clean_data()
        elif self._current_elem == 'text':
            attrs_str = ''.join(f' {k}="{v}"' for k, v in attrs.items())
            self._page_data['text'] += f'<{name}{attrs_str}>'
            return
        self._elements_stack.append(name)

    def endElement(self, name):
        if name == 'page':
            text = self._page_data['text']
            if self._page_data['ns'] == self._ARTICLE_NS and text:
                title = self._page_data['title']
                self._walker.article(title, text)
            self._clean_data()
        elif self._current_elem() == 'text' and name != 'text':
            self._page_data['text'] += f'</{name}>'
            return

        self._elements_stack.pop()

    def endDocument(self):
        self._walker.no_more_articles()


class TextWiktionaryWalker(WiktionaryWalker):
    def __init__(self, text_file=None, scheme=None, handler=None):
        super().__init__(scheme=scheme, handler=handler)
        self._clean_data()
        self.text_file = text_file
        self.delimiter = '*' * 100

    def _clean_data(self):
        self._page_data = {i: '' for i in ('text', 'title')}

    def _article_ready(self):
        try:
            text = self._page_data['text'][:-1]
        except KeyError:
            text = None
        if text:
            title = self._page_data['title']
            self.article(title, text)
        self._clean_data()

    def _walk_file(self, file):
        for line in file:
            if line.startswith(self.delimiter):
                self._article_ready()
                self._page_data['title'] = line[len(self.delimiter):-1]
            else:
                self._page_data['text'] += line
        self._article_ready()
        self.no_more_articles()

    def walk(self):
        with open(self.text_file) as file:
            self._walk_file(file)


class ArticleHandler:
    def article(self, title, get_parsed, raw):
        pass

    def no_more_articles(self):
        pass
