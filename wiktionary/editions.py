import functools
import re

import wiktionary.actions as actions
from .types import Construct, MultilinePattern, Sections

wiktionaryPatterns = {
    'pl': {
        'constructs': [
            Construct(re.compile(r'^== (?P<no_lang>[0-9 ]+) ==$'), actions.nonlanguage_header),
            Construct(re.compile(r'^== (?:.*) \(\{\{(?P<lang_name>.*)\}\}\) ==\s*$'), actions.language_header),
            Construct(re.compile(r'^== (?P<no_lang>.+) ==$'), actions.nonlanguage_header),
            Construct(re.compile(r'^\{\{(?!morfem)(?P<section_name>.*)\}\}(?:\s*(?P<content>.*))?$'), actions.section_header),
            Construct(re.compile(r'^(?:\[\[[\w-]+:[^]]+\]\]\s*)*$'), actions.set_interwiki),
            Construct(re.compile(r'^\{\{podobne2?\|[^}]+\}\}$'), actions.set_similar),
            Construct(re.compile(r'^\s*__TOC__\s*$'), actions.do_nothing),
            Construct(re.compile(r'^(?:\[\[(?:Plik|File):(?:[^\[\]]|(?:\[\[[^\]]+\]\]))+\]\]\s*)+$'), actions.do_nothing),
            Construct(re.compile(r'^#(?:redirect|patrz|tam) ?\[\[[^]]+\]\]$', re.IGNORECASE), actions.do_nothing),
            Construct(re.compile(r'^\{\{(?:do weryfikacji|dopracować|jidwer|linki)(?:\|[^}]+)?\}\}$'), actions.do_nothing),
            Construct(MultilinePattern(
                re.compile(r'^\{\{(?:Galeria|litera|ilustracja-języka)[^}]*$'),
                re.compile(r'^\|.*(?<!\}\})\s*?$'),
                re.compile(r'^(?!\{\{).*\}\}$')
            ), functools.partial(actions.log_multiline_template, delimiter='-')),
            Construct(MultilinePattern(
                re.compile(r'^\[\[Plik:.*?(?<!\]\])\s*?$'),
                re.compile(r'^.*?(?<!\]\])$'),
                re.compile(r'^.*\]\]$')
            ), functools.partial(actions.log_multiline_template, delimiter='~')),
            Construct(MultilinePattern(
                re.compile(r'^\{\| '),
                re.compile(r'^($|[|!]|\{\{\w+\}\})'),
                re.compile(r'^\|\}$')
            ), functools.partial(actions.log_multiline_template, delimiter='+')),
            Construct(re.compile('.*'), actions.lang_continue),
            Construct(re.compile('.*'), actions.nonlang_continue),
        ],
        'list': re.compile(r'^(?:\* [^:\n]+: [^\n]+)(?:\n\* [^:\n]+: [^\n]+)*$'),
        'ignoredpages': re.compile(r'^(?:([0-9]|\s)+|Słownik (?:(?:języka .*)|esperanto|jidysz|polskiego języka migowego|sanskrytu|slovio))$'),
        'featured_langs': ['użycie międzynarodowe', 'język polski'],
        'sections': {
            Sections.PRONUNCIATION: 'wymowa',
            Sections.MEANINGS: 'znaczenia',
            Sections.INFLECTION: 'odmiana',
            Sections.EXAMPLES: 'przykłady',
            Sections.SYNTAX: 'składnia',
            Sections.COLLOCATION: 'kolokacje',
            Sections.SYNONYMS: 'synonimy',
            Sections.ANTONYMS: 'antonimy',
            Sections.HYPERNYM: 'hiperonimy',
            Sections.HYPONYM: 'hiponimy',
            Sections.HOLONYM: 'holonimy',
            Sections.MERONYM: 'meronimy',
            Sections.RELATED: 'pokrewne',
            Sections.PHRASEOLOGY: 'frazeologia',
            Sections.ETYMOLOGY: 'etymologia',
            Sections.NOTES: 'uwagi',
            Sections.TRANSLATIONS: 'tłumaczenia',
            Sections.CITATION: 'źródła',
        }
    }
}
