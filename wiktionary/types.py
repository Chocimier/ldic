import collections
import enum

Construct = collections.namedtuple('Construct', ('pattern', 'action'))
MultilinePattern = collections.namedtuple('MultilinePattern',
    ('first', 'middle', 'last'))


class LoggingCategories(enum.Enum):
    UNEXPECTED = 'wiktionary.unexpected'
    NON_LANGUAGE = 'wiktionary.non-language'
    TEMPLATE = 'wiktionary.templates'
    DEBUG = 'wiktionary.debug'
    IGNORED_PAGE = 'wiktionary.ignored-pages'


class Sections(enum.Enum):
    PRONUNCIATION = 'pronunciation'
    MEANINGS = 'meanings'
    INFLECTION = 'inflection'
    EXAMPLES = 'examples'
    SYNTAX = 'syntax'
    COLLOCATION = 'collocation'
    SYNONYMS = 'synonyms'
    ANTONYMS = 'antonyms'
    HYPERNYM = 'hypernym'
    HYPONYM = 'hyponym'
    HOLONYM = 'holonym'
    MERONYM = 'meronym'
    RELATED = 'related'
    PHRASEOLOGY = 'phraseology'
    ETYMOLOGY = 'etymology'
    NOTES = 'notes'
    TRANSLATIONS = 'translations'
    CITATION = 'citation'


class WikiDefinition:
    def __init__(self, sections_map={}):
        self.title = None
        self.sections = {}
        self._sections_map = sections_map

    def section(self, index):
        try:
            index = Sections[index]
        except KeyError:
            pass
        try:
            key = self._sections_map[index]
        except KeyError:
            return None
        return (index, self.sections[key])


class WikiPage:
    def __init__(self):
        self.langs = {}
        self.nonlangs = []
        self.interwiki = None
        self.similar = None
        self.featured_langs = []


class ActionNotApplied(Exception):
    pass
