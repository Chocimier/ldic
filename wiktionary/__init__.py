import logging
import re

from .actions import log
from .types import ActionNotApplied, Construct, LoggingCategories, \
    MultilinePattern, WikiDefinition, WikiPage
from .editions import wiktionaryPatterns


def matches(expression, text, *, line_producer=None, page=''):
    if isinstance(expression, MultilinePattern):
        if not line_producer:
            raise ValueError('Multiline pattern passed without'
                + ' line_producer argument')
        if not matches(expression.first, text):
            return None
        next_line = line_producer()
        if next_line is not None:
            lines = [text, next_line]
        while (not matches(expression.last, lines[-1])
               and matches(expression.middle, lines[-1])):
            next_line = line_producer()
            if next_line is None:
                break
            lines.append(next_line)
        if matches(expression.last, lines[-1]):
            return lines
        log(LoggingCategories.UNEXPECTED,
            'Unclosed template{}: {}'.format(page, '\n'.join(lines)))
        return None
    return expression.search(text)


class WikiSectionParser:
    def _parse_template(self, text, page):
        parts = text.split('|')
        if parts[0] == 'morfem':
            return '<em>{}</em>'.format(parts[0])
        if parts[0] == 'wikipedia':
            if len(parts) == 1:
                parts.append(page)
            return ('<a href="http://pl.wikipedia.org/wiki/{}">{}</a>'
                .format(parts[1], parts[-1]))
        if parts[0].startswith('IPA'):
            return '/' + parts[1] + '/'
        if parts[0].startswith('SAMPA'):
            return 'SAMPA: /' + parts[1] + '/'
        if parts[0] == 'enPR':
            return parts[0] + ': /' + parts[1] + '/'
        if parts[0].startswith('audio'):
            return parts[1]
        return '<em>{}.</em>'.format(text)

    def _parse_lists(self, text):
        nl = '\n'
        result = ''
        list_open = None
        for line in text.split(nl):
            line_init = line[:2]
            if line_init in (': ', '* '):
                if not list_open:
                    result += '<ul>' + nl
                elif list_open != line_init:
                    result += '</ul>' + nl
                    result += '<ul>' + nl
                result += '<li>{}</li>\n'.format(line[2:])
                list_open = line_init
            else:
                if list_open:
                    result += '</ul>' + nl
                    list_open = None
                result += line + nl
        if list_open:
            result += '</ul>' + nl
        return result

    def parse(self, text, title):
        text = re.sub('<ref([^/>]*?>.*?)</ref>', lambda x: x.group(0) if re.search('ref', x.group(1)) else '', text)
        text = re.sub('<ref([^/>]*?)/>', lambda x: x.group(0) if re.search('ref', x.group(1)) else '', text)
        text = re.sub(r'\[\[([^\]]+)\]\]', lambda x: x.group(1).split('|')[-1], text)
        text = re.sub(r'\{\{([^{}]+)\}\}', lambda x: self._parse_template(x.group(1), page=title), text)
        text = re.sub("''([^']+)''", r'<strong>\1</strong>', text)
        text = self._parse_lists(text)
        return text


class WikiPageParser:
    def __init__(self, text, scheme, title=''):
        self._lines = text.split('\n')
        self._patterns = wiktionaryPatterns[scheme]
        self._parsed = None
        self._title = title
        self._current_lang = None
        self._current_nonlang = None
        self.current_section = None

    def get_line(self):
        try:
            line, self._lines = self._lines[0], self._lines[1:]
        except IndexError:
            return None
        return line

    @property
    def current_lang(self):
        return self._current_lang

    @current_lang.setter
    def current_lang(self, lang):
        if lang not in self._parsed.langs:
            self._parsed.langs[lang] = WikiDefinition(
                sections_map=self._patterns['sections']
            )
        self._current_lang = lang
        self._current_nonlang = None
        self.current_section = None

    @property
    def current_nonlang(self):
        return self._current_nonlang

    @current_nonlang.setter
    def current_nonlang(self, nonlang):
        if nonlang not in self._parsed.nonlangs:
            self._parsed.nonlangs.append(WikiDefinition())
        self._current_nonlang = nonlang
        self._current_lang = None
        self.current_section = None

    def extend_section(self, content):
        name = self.current_section
        if self.current_lang:
            definition = self._parsed.langs[self.current_lang]
        elif self.current_nonlang:
            definition = self._parsed.nonlangs[-1]
        else:
            log(LoggingCategories.UNEXPECTED,
                'Section outside language{}: {}'.format(
                    self.page_message(), name)
            )
            return True
        try:
            definition.sections[name] += '\n' + content
        except KeyError:
            definition.sections[name] = content
        self.current_section = name

    def page_message(self):
        if self._title:
            return ' on page "{}"'.format(self._title)
        else:
            return ''

    def parse(self):
        self._parsed = WikiPage()
        try:
            self._parsed.featured_langs = self._patterns['featured_langs']
        except KeyError:
            pass
        page_message = self.page_message()

        while self._lines:
            line = self.get_line()
            for construct in self._patterns['constructs']:
                match = matches(construct.pattern, line, page=page_message,
                    line_producer=self.get_line)
                if not match:
                    continue
                try:
                    construct.action(match, self, line=line)
                    break
                except ActionNotApplied:
                    pass
            else:
                log(LoggingCategories.UNEXPECTED,
                    'Unexpected line{}: »{}«'.format(self.page_message(), line))
        for lang in self._parsed.langs:
            self._parsed.langs[lang].sections = {k: v
                for k, v in self._parsed.langs[lang].sections.items()
                if v}

    def get_parsed(self):
        if matches(self._patterns['ignoredpages'], self._title):
            log(LoggingCategories.IGNORED_PAGE, self._title, logging.INFO)
            return None
        if self._parsed is None:
            self.parse()

        return self._parsed
