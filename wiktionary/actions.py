import logging
from .types import ActionNotApplied, LoggingCategories


def log(category, message, level=logging.WARNING):
    if isinstance(category, LoggingCategories):
        category = category.value
    logging.getLogger(category).log(level, message)


def do_nothing(*args, **kwargs):
    del args, kwargs
    pass


def set_interwiki(match, parser, *, line, **kwargs):
    del match, kwargs
    parser._parsed.interwiki = line


def set_similar(match, parser, *, line, **kwargs):
    del match, kwargs
    parser._parsed.similar = line


def language_header(match, parser, **kwargs):
    del kwargs
    lang = match.groupdict()['lang_name']
    parser.current_lang = lang


def nonlanguage_header(match, parser, **kwargs):
    nonlang = match.groupdict()['no_lang']
    parser.current_nonlang = nonlang
    if kwargs.get('log', False):
        log(LoggingCategories.UNEXPECTED,
            'Non-language definition'
            + parser.page_message()
            )


def section_header(match, parser, **kwargs):
    del kwargs
    name = match.groupdict()['section_name']
    content = match.groupdict()['content']
    parser.current_section = name
    if content:
        parser.extend_section(content)


def lang_continue(match, parser, *, line, **kwargs):
    del match, kwargs
    if parser.current_lang and parser.current_section:
        parser.extend_section(line)
    else:
        raise ActionNotApplied()


def nonlang_continue(match, parser, *, line, **kwargs):
    del match, kwargs
    if parser.current_nonlang and parser.current_section:
        parser.extend_section(line)
    else:
        raise ActionNotApplied()


def log_multiline_template(match, parser, *, delimiter='?', **kwargs):
    del kwargs
    log(LoggingCategories.TEMPLATE,
        (delimiter*100) + parser.page_message() + '\n' + '\n'.join(match),
        logging.INFO)
