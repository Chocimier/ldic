#!/usr/bin/env python3
from bottle import default_app, request, route, run, static_file
from ldic import LocalDictionary
from ldic.datasources import SqliteDataSource
import logging
import sys


root_url = '/ldic'
data_source = SqliteDataSource('dictionary.sqlite3')
dictionary = LocalDictionary(data_source)


@route('/')
@route('/word', method=('GET', 'POST'))
def package():
    word = getattr(request.params, 'word', '')
    return dictionary.page(word, root_url=root_url)


@route('/static/<filename>')
def static(filename):
    return static_file(filename, 'static')


class UrlPrefixMiddleware(object):
    def __init__(self, prefix, app):
        self._app = app
        self._prefix = prefix
        self._prefix_len = len(prefix)

    def __call__(self, e, h):
        if e['PATH_INFO'].startswith(self._prefix):
            e['PATH_INFO'] = e['PATH_INFO'][self._prefix_len:]
        return self._app(e, h)


# https://www.bottlepy.org/docs/dev/recipes.html#ignore-trailing-slashes
class StripSlashMiddleware(object):
    def __init__(self, app):
        self._app = app

    def __call__(self, e, h):
        e['PATH_INFO'] = e['PATH_INFO'].rstrip('/')
        return self._app(e, h)


application = StripSlashMiddleware(
    UrlPrefixMiddleware(root_url,
    default_app()
))

kwargs = {}

try:
    kwargs['server'] = sys.argv[1]
except IndexError:
    pass

run(app=application, reloader=True, **kwargs)
