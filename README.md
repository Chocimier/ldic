# Uruchamianie
1. Zainstaluj Pythona 3 i moduły z requirements.txt
2. Pobierz wersję `pages-articles.xml.bz2` zrzutu Wikisłownika ze strony
https://dumps.wikimedia.org/plwiktionary/
3. Utwórz bazę: `python3 -m ldic.mkdbase pl plwiktionary-*-pages-articles.xml`
4. Uruchom słownik: `./serve.py`.
5. Odwiedź adres wypisany przez poprzednie polecenie.
